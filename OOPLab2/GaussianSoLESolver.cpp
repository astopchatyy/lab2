#include "GaussianSoLESolver.h"

GaussianSoLESolver::GaussianSoLESolver() : rowsFirstNonZero(std::vector<int>()), curColumn(0), curRow(0){}

void GaussianSoLESolver::rowsFirstNonZeroUpdate()
{
	for (int i = 0; i < operationMatrix.Rows(); i++)
	{
		for (rowsFirstNonZero[i] = 0; rowsFirstNonZero[i] < operationMatrix.Columns() && operationMatrix[i][rowsFirstNonZero[i]] == 0; rowsFirstNonZero[i]++);
	}
}
bool GaussianSoLESolver::init(Matrix origin) 
{
	initialized = true;
	originMatrix = operationMatrix = origin;
	curColumn = 0;
	curRow = 0;
	rowsFirstNonZero = std::vector<int>(operationMatrix.Rows());
	rowsFirstNonZeroUpdate();
	process();
	operationMatrix.normalizeZeros();
	return true;
}
void GaussianSoLESolver::process()
{
	while (curColumn < operationMatrix.Columns() - 1 && curRow < operationMatrix.Rows())
	{
		stepToTriangle();
		rowsFirstNonZeroUpdate();
	}
}
void GaussianSoLESolver::stepToTriangle()
{
	int pos = (int)(std::find(rowsFirstNonZero.begin(), rowsFirstNonZero.end(), curColumn) - rowsFirstNonZero.begin());
	if (pos < operationMatrix.Rows())
	{
		std::swap(operationMatrix[curRow], operationMatrix[pos]);
		std::swap(rowsFirstNonZero[curRow], rowsFirstNonZero[pos]);
		pos = curRow;
		curRow++;
		long double el = operationMatrix[pos][curColumn];
		operationMatrix[pos] /= el;
		for (int i = 0; i < operationMatrix.Rows(); i++)
		{
			if (i != pos && !compareTo(operationMatrix[i][curColumn],0))
			{
				el = operationMatrix[i][curColumn];
				for (int k = 0; k < operationMatrix.Columns(); k++)
				{
					if (operationMatrix[i][k] != 0 && -operationMatrix[i][k] != operationMatrix[pos][k] * (-el) && operationMatrix[i][k] + operationMatrix[pos][k] * (-el) == 0)
					{
						std::cout << operationMatrix[i][k] << "  " << el << "  " << operationMatrix[pos][k] << '\n';
					}
				}
				operationMatrix[i] += operationMatrix[pos] * (-el);
			}
		}
	}
	curColumn++;
}
bool GaussianSoLESolver::resultToVector(std::vector<long double>& result)
{
	if (operationMatrix.Rows() != operationMatrix.Columns() - 1 || !initialized)
		return false;
	result = std::vector<long double>(operationMatrix.Rows());
	for (int i = 0; i < operationMatrix.Rows(); i++)
	{
		for (int j = 0; j < operationMatrix.Columns()-1; j++)
		{
			if ((i == j && !compareTo(operationMatrix[i][j], 1)) || (i != j && !compareTo(operationMatrix[i][j], 0)))
				return false;
		}
		result[i] = operationMatrix[i][operationMatrix.Columns() - 1];
	}
	return true;
}
