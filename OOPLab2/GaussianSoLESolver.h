#pragma once

#include "Matrix.h"
#include "SoLESolver.h"
#include <vector>
#include <algorithm>
#include "Functions.h"
#include <cmath>

class GaussianSoLESolver : public SoLESolver
{
private:
	int curColumn, curRow;
	std::vector<int> rowsFirstNonZero;
	void process();
	void rowsFirstNonZeroUpdate();
	void stepToTriangle();

public:
	GaussianSoLESolver();
	bool init(Matrix origin);
	bool resultToVector(std::vector<long double>& result);
};

