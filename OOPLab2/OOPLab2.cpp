﻿#include "Matrix.h"
#include "GaussianSoLESolverV2.h"
#include "JacobiEigenFinder.h"
#include "Functions.h"
#include "SoLEBuilder.h"
#include "SimmetrialMatrixBuilder.h"
#include "LinearRegressionModel.h"
#include "DeviationGenerator.h"
#include "ClassifierModel.h"
#include "MnistMatrixLoader.h"


int main()
{
	MnistMatrixLoader loader;
	loader.init("D:\\Документы\\Загрузки\\MNIST\\train-images.idx3-ubyte", "D:\\Документы\\Загрузки\\MNIST\\train-labels.idx1-ubyte");

	SoLESolver* solver = new GaussianSoLESolverV2();

	RegressionModel* regression = new LinearRegressionModel(solver);

	ClassifierModel	classifier(regression, 28 * 28);

	Matrix zerosMatrix = loader[0];
	Matrix onesMatrix = loader[1];

	Matrix zeroClass = Matrix(zerosMatrix.Rows() + onesMatrix.Rows(), zerosMatrix.Columns());
	for (int i = 0; i < zerosMatrix.Rows(); i++)
	{
		zeroClass[i] = zerosMatrix[i];
		zeroClass[i][0] = 1;
	}
	for (int i = 0; i < onesMatrix.Rows(); i++)
	{
		zeroClass[zerosMatrix.Rows() + i] = onesMatrix[i];
		zeroClass[zerosMatrix.Rows() + i][0] = -1;
	}
	Matrix oneClass = zeroClass;

	for (int i = 0; i < oneClass.Rows(); i++)
	{
		oneClass[i][0] = -oneClass[i][0];
	}

	for (int i = 0; i < 2; i++)
	{
		classifier.initClass(loader[i], i);
	}

	loader.init("D:\\Документы\\Загрузки\\MNIST\\t10k-images.idx3-ubyte", "D:\\Документы\\Загрузки\\MNIST\\t10k-labels.idx1-ubyte");

	Matrix zeros = loader[0];

	std::cout << classifier.reviewClassifyingCorrectness(zeros) << "/" << zeros.Rows();


}
/*
4
6 1
5 2
7 3
10 4

1.4х + 3.5


4 1 -1
1 4 -1
-1 -1 4

3 3 6

*/