#pragma once

#include "Matrix.h"
#include "MatrixBuilder.h"
#include <cstdlib>
#include <ctime>
#include <vector>

class SoLEBuilder : public MatrixBuilder
{
private:
	std::vector<long double> getRandomVector(int len);
	void setRandomCoefficients(Matrix& matrix);
public:
	SoLEBuilder(int valueScatter = 10, int minSize = 1, int maxSize = 10);
	Matrix loadFromStream(std::istream& inStream);
	Matrix generateRandom();
	Matrix generateRandom(int variablesCount);
};

