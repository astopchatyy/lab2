#include "Functions.h"

const long double DELTA = 1e-15;

bool compareTo(long double toComp, long double with)
{
	return(toComp < with + DELTA && toComp > with - DELTA);
}

int randBetween(int from, int to)
{
	return (rand() % (to - from + 1)) + from;
}
long double randDoubleBetween(int from, int to)
{
	return ((long double)randBetween(100 * from, 100 * to)) / 100;
}
int readIntFromBytes(std::istream& InStream)
{
	int result = 0;
	unsigned char byte = 0;
	for (int i = 0; i < 4; i++)
	{
		InStream >> byte;
		result = (result << 8) + byte;
	}
	return result;
}

