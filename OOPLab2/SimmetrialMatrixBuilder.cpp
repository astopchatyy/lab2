#include "SimmetrialMatrixBuilder.h"


SimmetrialMatrixBuilder::SimmetrialMatrixBuilder(int valueScatter, int minSize, int maxSize) : MatrixBuilder(valueScatter, minSize, maxSize)
{

}
Matrix SimmetrialMatrixBuilder::generateRandom()
{
	int matrixSize = randBetween(minSize, maxSize);
	return generateRandom(matrixSize);
}
Matrix SimmetrialMatrixBuilder::generateRandom(int matrixSize)
{
	Matrix matrix(matrixSize, matrixSize);
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = i; j < matrixSize; j++)
		{
			int val = randBetween(-valueScatter, valueScatter);
			matrix[j][i] = val;
			matrix[i][j] = val;
		}
	}
	return matrix;
}
Matrix SimmetrialMatrixBuilder::loadFromStream(std::istream& inStream)
{
	int rows;
	inStream >> rows;
	Matrix result(rows, rows);
	inStream >> result;
	if (!result.isSimmetrial())
		return Matrix();
	return result;
}