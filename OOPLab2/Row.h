#pragma once

#include <iostream>
#include <vector>
#include <iomanip>
#include <malloc.h>
#include "Functions.h"

class Row
{
private:
	std::unique_ptr<long double[]> rowValues;
	int len;


public:
	void free();
	Row(std::initializer_list<long double> list);
	Row(int len, std::initializer_list<long double> list);
	Row(int len);
	Row(const Row& row);
	Row();
	Row operator=(const Row& right);
	long double& operator[](int index);
	friend std::ostream& operator <<(std::ostream& outStream, Row& matrix);
	friend std::istream& operator >>(std::istream& inStream, Row& matrix);
	Row operator+(const Row& right) const;
	Row operator-(const Row& right) const;
	bool operator==(Row& right);
	bool operator!=(Row& right);
	Row operator *(long double right)  const;
	Row operator /(long double right)  const;
	Row operator+=(const Row& right);
	Row operator-=(const Row& right);
	Row operator*=(const long double right);
	Row operator/=(const long double right);
};

