#include "Matrix.h"


Row* Matrix::Begin()
{
	return data.get();
}
int Matrix::Columns()
{
	return columns;
}
int Matrix::Rows()
{
	return rows;
}
Matrix::Matrix(int rows, int columns, int diagon) : rows(rows), columns(columns)
{
	if (rows < 1 || columns < 1)
		throw std::length_error("Invalid matrix size");
	data = std::make_unique<Row[]>(rows);
	for (int i = 0; i < rows; i++)
	{
		data[i] = Row(columns);
		if (i < columns)
			data[i][i] = diagon;
	}
}
Matrix::Matrix(const Matrix& matrix) : rows(matrix.rows), columns(matrix.columns)
{
	data = std::make_unique<Row[]>(rows);
	for (int i = 0; i < rows; i++)
	{
		data[i] = matrix.data[i];
	}
}
Matrix::Matrix()
{
	rows = 0;
	columns = 0;
	data = NULL;
}
Matrix Matrix::operator=(const Matrix& right)
{


	rows = right.rows;
	columns = right.columns;
	data.reset();
	data = std::make_unique<Row[]>(right.rows);
	for (int i = 0; i < right.rows; i++)
	{
		data[i] = right.data[i];
	}
	return *this;
}
Row& Matrix::operator[](int index)
{
	if (index < 0 || index >= rows)
		throw std::out_of_range("Index out of range");
	return data[index];
}
long double& Matrix::operator[](std::pair<int, int> position)
{
	int i1 = position.first, i2 = position.second;
	if (i1 < 0 || i1 >= rows || i2 < 0 || i2 >= columns)
		throw std::out_of_range("Index out of range");
	return data[i1][i2];
}
std::ostream& operator <<(std::ostream& outStream, const Matrix& matrix)
{
	for (int i = 0; i < matrix.rows; i++)
	{
		outStream <<  matrix.data[i] <<  '\n';
	}
	return outStream;
}
std::istream& operator >>(std::istream& inStream, Matrix& matrix)
{
	for (int i = 0; i < matrix.rows; i++)
	{
		inStream >> matrix[i];
	}
	return inStream;
}
Matrix Matrix::operator+(const Matrix& right) const
{
	if (this->rows != right.rows || this->columns != right.columns)
		throw std::out_of_range("Incompatible matrixes");
	Matrix result(*this);
	for (int i = 0; i < rows; i++)
	{
		result[i] += right.data[i];
	}
	return result;
}
Matrix Matrix::operator-(const Matrix& right) const 
{
	if (this->rows != right.rows || this->columns != right.columns)
		throw std::invalid_argument("Incompatible matrixes");
	Matrix result(*this);
	for (int i = 0; i < rows; i++)
	{
		result[i] -= right.data[i];
	}
	return result;
}
bool Matrix::operator==(const Matrix& right)
{
	if (this->rows != right.rows || this->columns != right.columns)
		return false;
	for (int i = 0; i < rows; i++)
	{
		
		if ((*this)[i] != right.data[i])
			return false;
	}
	return true;
}
Matrix Matrix::operator *(const Matrix& right)  const
{
	if (this->columns != right.rows)
		throw std::out_of_range("Incompatible matrixes");
	Matrix result(this->rows, right.columns);
	for (int i = 0; i < this->rows; i++)
	{
		for (int j = 0; j < right.columns; j++)
		{
			for (int k = 0; k < this->columns; k++)
			{
				result[i][j] += this->data[i][k] * right.data[k][j];
			}
		}
	}
	return result;
}
Matrix Matrix::operator *(long double right)  const
{
	Matrix result(*this);
	for (int i = 0; i < rows; i++)
	{
		result[i] *= right;
	}
	return result;
}
Matrix Matrix::operator /(long double right)  const
{
	Matrix result(*this);
	for (int i = 0; i < rows; i++)
	{
		result[i] /= right;
	}
	return result;
}
Matrix Matrix::operator+=(const Matrix& right)
{
	*this = *this + right;
	return *this;
}
Matrix Matrix::operator-=(const Matrix& right)
{
	*this = *this - right;
	return *this;
}
Matrix Matrix::operator*=(const Matrix& right)
{
	*this = *this * right;
	return *this;
}
Matrix Matrix::operator*=(long double right)
{
	*this = *this * right;
	return *this;
}
Matrix Matrix::operator/=(long double right)
{
	*this = *this / right;
	return *this;
}
Matrix Matrix::operator~()
{
	Matrix transposed(columns, rows);
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			transposed[j][i] = (*this)[i][j];
		}
	}
	return transposed;
}
void Matrix::normalizeZeros()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			if (compareTo((*this)[i][j], 0))
				(*this)[i][j] = 0;
		}
	}
}
bool Matrix::isSimmetrial()
{
	if (rows != columns || rows == 0)
		return false;
	for (int i = 1; i < rows; i++)
	{
		for (int j = 0; j < i; j++)
		{
			if ((*this)[i][j] != (*this)[j][i])
				return false;
		}
	}
	return true;
}
