#pragma once

#include <ctime>
#include <cstdlib>
#include <iostream>

bool compareTo(long double toComp, long double with);
int randBetween(int from, int to);
long double randDoubleBetween(int from, int to);
int readIntFromBytes(std::istream& InStream);

