#pragma once

#include "EigenFinder.h"
#include "Functions.h"

class JacobiEigenFinder : public EigenFinder
{

private:
	void process();
	std::pair<int, int> getMaxAbsIndex();
	
public:
	JacobiEigenFinder();
	bool init(Matrix origin);
	bool resultToVector(std::vector<long double>& result);
};

