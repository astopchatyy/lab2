#pragma once

#include "Row.h"
#include "Functions.h"

class Matrix
{
private:
	std::unique_ptr<Row[]> data;
	int rows, columns;
public:
	int Columns();
	int Rows();
	Row* Begin();
	Matrix(int rows, int columns, int diagon = 0);
	Matrix(const Matrix& matrix);
	Matrix();
	Matrix operator=(const Matrix& right);
	Row& operator[](int index);
	long double& operator[](std::pair<int,int> position);
	friend std::ostream& operator <<(std::ostream& outStream, const Matrix& matrix);
	friend std::istream& operator >>(std::istream& inStream, Matrix& matrix);
	Matrix operator+(const Matrix& right) const;
	Matrix operator-(const Matrix& right) const;
	bool operator ==(const Matrix& right);
	Matrix operator *(const Matrix& right) const;
	Matrix operator *(long double right) const;
	Matrix operator /(long double right) const;
	Matrix operator +=(const Matrix& right);
	Matrix operator -=(const Matrix& right);
	Matrix operator *=(const Matrix& right);
	Matrix operator *=(long double right);
	Matrix operator /=(long double right);
	Matrix operator~();
	void normalizeZeros();
	bool isSimmetrial();
};

