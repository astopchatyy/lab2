#pragma once

#include "Matrix.h"

class MatrixProcessor
{
protected:
	bool initialized;
	Matrix operationMatrix, originMatrix;
public:
	bool Initialized();
	MatrixProcessor();
	virtual bool init(Matrix origin) = 0;
	virtual Matrix getOriginMatrix();
	virtual Matrix getResultMatrix();
	virtual bool resultToVector(std::vector<long double>& result) = 0;

};

