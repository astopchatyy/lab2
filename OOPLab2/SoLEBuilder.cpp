#include "SoLEBuilder.h"



SoLEBuilder::SoLEBuilder(int valueScatter, int minSize, int maxSize ) : MatrixBuilder(valueScatter, minSize, maxSize)
{

};
Matrix SoLEBuilder::loadFromStream(std::istream& inStream)
{
	int rows, columns;
	inStream >> rows >> columns;
	Matrix result(rows, columns);
	inStream >> result;
	return result;
}
std::vector<long double> SoLEBuilder::getRandomVector(int len)
{

	std::vector<long double> vars(len);
	for (int i = 0; i < len; i++)
	{
		vars[i] = randBetween(-valueScatter, valueScatter);
	}
	return vars;
}
void SoLEBuilder::setRandomCoefficients(Matrix& matrix)
{
	for (int i = 0; i < matrix.Rows(); i++)
	{
		for (int j = 0; j < matrix.Columns() - 1; j++)
		{
			matrix[i][j] = randBetween(-valueScatter, valueScatter);
		}
	}
}
Matrix SoLEBuilder::generateRandom()
{
	int variables = randBetween(minSize, maxSize);
	return generateRandom(variables);
}

Matrix SoLEBuilder::generateRandom(int variablesCount)
{
	std::vector<long double> vars = getRandomVector(variablesCount);
	Matrix matrix(variablesCount, variablesCount + 1);
	setRandomCoefficients(matrix);
	long double rowResult;
	for (int i = 0; i < variablesCount; i++)
	{
		rowResult = 0;
		for (int j = 0; j < variablesCount; j++)
		{
			rowResult += matrix[i][j] * vars[j];
		}
		matrix[i][variablesCount ] = rowResult;
	}
	return matrix;
}