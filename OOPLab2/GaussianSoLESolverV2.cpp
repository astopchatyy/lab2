#include "GaussianSoLESolverV2.h"

GaussianSoLESolverV2::GaussianSoLESolverV2() : curColumn(0), curRow(0) {}

bool GaussianSoLESolverV2::init(Matrix origin)
{
	initialized = true;
	originMatrix = operationMatrix = origin;
	curColumn = 0;
	curRow = 0;
	process();
	operationMatrix.normalizeZeros();
	return true;
}
void GaussianSoLESolverV2::process()
{

	for (int i = 0; i < operationMatrix.Rows(); i++)
	{
		long double el = operationMatrix[i][i];
		for (int j = i + 1; j < operationMatrix.Rows() && el == 0; j++)
		{
			if (operationMatrix[j][i] != 0)
			{
				std::swap(operationMatrix[j], operationMatrix[i]);
				el = operationMatrix[i][i];
			}
		}
		if (el == 0)
		{
			std::cout << 1;
		}
		operationMatrix[i] /= el;
		for (int j = 0; j < operationMatrix.Rows(); j++)
		{
			if (j != i)
			{
				operationMatrix[j] -= operationMatrix[i] * operationMatrix[j][i];
			}
		}
	}
}
bool GaussianSoLESolverV2::resultToVector(std::vector<long double>& result)
{
	if (operationMatrix.Rows() != operationMatrix.Columns() - 1 || !initialized)
		return false;
	result = std::vector<long double>(operationMatrix.Rows());
	for (int i = 0; i < operationMatrix.Rows(); i++)
	{
		for (int j = 0; j < operationMatrix.Columns() - 1; j++)
		{
			if ((i == j && !compareTo(operationMatrix[i][j], 1)) || (i != j && !compareTo(operationMatrix[i][j], 0)))
				return false;
		}
		result[i] = operationMatrix[i][operationMatrix.Columns() - 1];
	}
	return true;
}