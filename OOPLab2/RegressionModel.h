#pragma once

#include "Matrix.h"
#include "GaussianSoLESolver.h"

class RegressionModel
{
protected:
	SoLESolver* solver;
	Matrix data;
	Matrix result;
	bool initialized;
public:
	bool Initialized();
	RegressionModel(SoLESolver* solver);
	virtual bool init(Matrix data) = 0;
	virtual Matrix getResult() = 0;
	virtual std::vector<long double> resultToVector() = 0;
};

