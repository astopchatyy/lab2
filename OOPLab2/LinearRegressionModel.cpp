#include "LinearregressionModel.h"


LinearRegressionModel::LinearRegressionModel(SoLESolver* solver) : RegressionModel(solver)
{

}
bool LinearRegressionModel::init(Matrix origin)
{
	initialized = true;
	setData(origin);
	process();
	return true;
}
void LinearRegressionModel::process()
{
	Matrix A = data;
	Matrix Y = Matrix(data.Rows(), 1);

	for (int i = 0; i < data.Rows(); i++)
	{
		Y[i][0] = A[i][0];
		A[i][0] = 1;
	}

	Matrix squareMatrix = (~A) * A;

	Matrix emptySquareMatrix(squareMatrix.Rows(), squareMatrix.Rows(), 1);

	Matrix extended = Matrix(squareMatrix.Rows(), squareMatrix.Rows() * 2);

	for (int i = 0; i < squareMatrix.Rows(); i++)
	{
		for (int j = 0; j < squareMatrix.Rows(); j++)
		{
			extended[i][j] = squareMatrix[i][j];
			extended[i][j + squareMatrix.Rows()] = emptySquareMatrix[i][j];
		}
	}

	solver->init(extended);
	extended = solver->getResultMatrix();

	Matrix squareResult = Matrix(squareMatrix.Rows(), squareMatrix.Rows());

	for (int i = 0; i < squareMatrix.Rows(); i++)
	{
		for (int j = 0; j < squareMatrix.Rows(); j++)
		{
			squareResult[i][j] = extended[i][j + squareMatrix.Rows()];
		}
	}

	setResult(squareResult * (~A) * Y);
}
Matrix LinearRegressionModel::getResult()
{
	if (!initialized)
		throw std::logic_error("You should to init the model");
	return { result };
}

std::vector<long double> LinearRegressionModel::resultToVector()
{
	std::vector<long double> resultVector;
	for (int i = 0; i < result.Rows(); i++)
	{
		resultVector.push_back(result[i][0]);
	}
	return resultVector;
}
void LinearRegressionModel::setData(Matrix origin)
{
	isMinor = std::vector<bool>(origin.Columns());
	minorCounter = 0;
	for (int i = 1; i < origin.Columns(); i++)
	{
		isMinor[i] = true;
		for (int j = 1; j < origin.Rows() && isMinor[i]; j++)
		{
			isMinor[i] = isMinor[i] && origin[j][i] == origin[j-1][i];
		}
		if (isMinor[i])
			minorCounter++;
	}
	data = Matrix(origin.Rows(), origin.Columns() - minorCounter);
	int shift = 0;
	for (int i = 0; i < origin.Columns(); i++)
	{
		if (isMinor[i])
		{
			shift++;
		}
		else
		{
			for (int j = 0; j < origin.Rows(); j++)
			{
				data[j][i-shift] = origin[j][i];
			}
		}
	}
}
void LinearRegressionModel::setResult(Matrix rawResult)
{
	result = Matrix(rawResult.Rows() + minorCounter, 1);
	int shift = 0;
	for (int i = 0; i < rawResult.Rows(); i++)
	{
		while (isMinor[i + shift])
		{

			result[i + shift][0] = 0;
			shift++;
		}
		result[i + shift] = rawResult[i];
	}
}