#include "MnistMatrixLoader.h"
#include <fstream>



MnistMatrixLoader::MnistMatrixLoader() : initialized(false), amount(0), rows(0), columns(0) {}

bool MnistMatrixLoader::init(std::string imagesPath, std::string labelsPath)
{
    std::ifstream imagesReader;
    std::ifstream labelsReader;
    imagesReader.open(imagesPath, std::ios::binary);
    labelsReader.open(labelsPath);
    if (imagesReader.is_open() && labelsReader.is_open())
    {
        int imagesControlValue, labelsControlValue, imagesAmount, labelsAmount, rows, columns;
        unsigned char byte, label;
        imagesControlValue = readIntFromBytes(imagesReader);
        imagesAmount = readIntFromBytes(imagesReader);
        rows = readIntFromBytes(imagesReader);
        columns = readIntFromBytes(imagesReader);
        labelsControlValue = readIntFromBytes(labelsReader);
        labelsAmount = readIntFromBytes(labelsReader);
        if (imagesControlValue == IMAGES_CONTROL && labelsControlValue == LABELS_CONTROL && imagesAmount == labelsAmount)
        {
            amount = 5000;
            this->rows = rows;
            this->columns = columns;

            int simpleNumberAmount[9] = { 0 };
            std::vector<int> labels;
            
            for (int i = 0; i < amount; i++)
            {
                labelsReader >> byte;
                simpleNumberAmount[byte]++;
                labels.push_back(byte);
            }

            initDatasetMatrixes(simpleNumberAmount);

            for (int i = 0; i < amount; i++)
            {
                label = labels[i];
                simpleNumberAmount[label]--;
                int index = simpleNumberAmount[label];

                datasetMatrixes[label][index][0] = label;
                for (int j = 1; j <= rows * columns; j++)
                {
                    imagesReader >> byte;
                    datasetMatrixes[label][index][j] = byte > 0 ? 1 : 0;
                }

            }
            initialized = true;
            return true;
        }
    }
    return false;
}

void MnistMatrixLoader::initDatasetMatrixes(int matrixSizes[10])
{
    for (int i = 0; i < 9; i++)
    {
        datasetMatrixes[i] = Matrix(matrixSizes[i], rows * columns + 1);
    }
}

Matrix MnistMatrixLoader::operator[](int index)
{
    if(index > 9 || index < 0)
        throw std::out_of_range("Index out of range");

    return datasetMatrixes[index];
}
