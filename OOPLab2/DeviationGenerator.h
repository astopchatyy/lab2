#pragma once

#include <vector>
#include <cstdlib>
#include <ctime>
#include "Functions.h"
#include "Matrix.h"


class DeviationGenerator
{
private:
	long double deviationScatter;
	const long double MINX = 0;
	const long double MAXX = 20;
public:
	DeviationGenerator(long double deviationScatter = 2);
	Matrix deviationFromLine(long double a, long double b, int len = 100);
	Matrix deviationFromStream(std::istream& inStream);
	Matrix deviationFromMultyDem(std::vector<long double> params, int len = 100);
};

