#pragma once

#include "RegressionModel.h"


class LinearRegressionModel : public RegressionModel
{
private:
	void process();
	void setData(Matrix origin);
	void setResult(Matrix result);
	std::vector<bool> isMinor;
	int minorCounter;
public:
	LinearRegressionModel(SoLESolver* solver);

	virtual bool init(Matrix data);
	virtual Matrix getResult();
	virtual std::vector<long double> resultToVector();
};

