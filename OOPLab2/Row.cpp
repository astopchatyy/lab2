#include "Row.h"


Row::Row(std::initializer_list<long double> list) :len(list.size())
{

	rowValues = std::make_unique<long double[]>(len);

	const long double* ptr = list.begin();
	for (int i = 0; i < len; i++)
	{
		rowValues[i] = *ptr;
		ptr++;
	}
}
Row::Row(int len, std::initializer_list<long double> list): len(len)
{
	rowValues = std::make_unique<long double[]>(len);

	const long double* ptr = list.begin();
	for (int i = 0; i < len && i < list.size(); i++)
	{
		rowValues[i] = *ptr;
		ptr++;
	}
}
Row::Row(int len) : len(len)
{
	rowValues = std::make_unique<long double[]>(len);
	for (int i = 0; i < len; i++)
		rowValues[i] = 0;
}
Row::Row(const Row& row) : len(row.len)
{
	rowValues = std::make_unique<long double[]>(len);
	for (int i = 0; i < len; i++)
	{
		rowValues[i] = row.rowValues[i];
	}
}
Row::Row()
{
	rowValues = std::unique_ptr<long double[]>();
	len = 0;
}
Row Row::operator=(const Row& right)
{
	len = right.len;
	rowValues = std::make_unique<long double[]>(len);
	for (int i = 0; i < len; i++)
	{
		rowValues[i] = right.rowValues[i];
	}
	return *this;
}
long double& Row::operator[](int index)
{
	if (index < 0 || index >= len)
		throw std::out_of_range("Index out of range");
	return rowValues[index];
}
std::ostream& operator <<(std::ostream& outStream, Row& row)
{
	for (int i = 0; i < row.len; i++)
	{
		
		outStream << std::right << std::setw(3) << abs((int)row[i]) * (row[i] > 0 ? 1 : -1);
		int val = (int)abs((row[i] - (int)row[i]) * 100);
		if (val > 0)
		{
			outStream << ".";
			
			if (val < 10)
				outStream << "0";
			outStream << val << " ";
		}
			
		else
			outStream << "    ";
	}
	return outStream;
}
std::istream& operator >>(std::istream& inStream, Row& row)
{
	for (int i = 0; i < row.len; i++)
	{

		inStream >> row[i];
	}
	return inStream;
}
Row Row::operator+(const Row& right) const
{
	if (this->len != right.len)
		throw std::out_of_range("Incompatible Rows");
	Row result(*this);
	for (int i = 0; i < len; i++)
	{
			result[i] += right.rowValues[i];
	}
	return result;
}
Row Row::operator-(const Row& right) const
{
	if (this->len != right.len)
		throw std::out_of_range("Incompatible Rows");
	Row result(*this);
	for (int i = 0; i < len; i++)
	{
		result[i] -= right.rowValues[i];
	}
	return result;
}
bool Row::operator==(Row& right)
{
	if (this->len != right.len)
		return false;
	for (int i = 0; i < len; i++)
	{
		
		if ((*this)[i] != right[i])
			return false;
	}
	return true;
}
bool Row::operator!=(Row& right)
{
	return !((*this) == right);
}
Row Row::operator *(long double right) const
{
	Row result(*this);
	for (int i = 0; i < len; i++)
	{
		result[i] *= right;
	}

	return result;
}
Row Row::operator /(long double right) const
{
	Row result(*this);
	for (int i = 0; i < len; i++)
	{
		result[i] /= right;
	}
	return result;
}
Row Row::operator+=(const Row& right)
{
	*this = *this + right;
	return *this;
}
Row Row::operator-=(const Row& right)
{
	*this = *this - right;
	return *this;
}
Row Row::operator*=(const long double right)
{
	*this = *this * right;
	return *this;
}
Row Row::operator/=(const long double right)
{
	*this = *this / right;
	return *this;
}