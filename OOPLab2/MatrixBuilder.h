#pragma once

#include "Matrix.h"
#include "Functions.h"

class MatrixBuilder
{
protected:
	int minSize, maxSize, valueScatter;
public:
	MatrixBuilder(int valueScatter = 10, int minSize = 1, int maxSize = 10);
	virtual Matrix loadFromStream(std::istream& inStream) = 0;
	virtual Matrix generateRandom() = 0;
	virtual Matrix generateRandom(int variables) = 0;
};

