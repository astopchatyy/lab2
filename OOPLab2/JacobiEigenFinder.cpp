#include "JacobiEigenFinder.h"





JacobiEigenFinder::JacobiEigenFinder() {}
bool JacobiEigenFinder::init(Matrix origin)
{
	operationMatrix = originMatrix = origin;
	if (!operationMatrix.isSimmetrial())
		return false;
	initialized = true;
	process();
	operationMatrix.normalizeZeros();
	return true;
}
std::pair<int, int> JacobiEigenFinder::getMaxAbsIndex()
{
	int maxAbsI = 1, maxAbsJ = 0;
	for (int i = 1; i < operationMatrix.Rows(); i++)
	{
		for (int j = 0; j < i; j++)
		{
			if (abs(operationMatrix[i][j]) > abs(operationMatrix[maxAbsI][maxAbsJ]))
			{
				maxAbsI = i;
				maxAbsJ = j;
			}
		}
	}
	return { maxAbsJ, maxAbsI };
}

void JacobiEigenFinder::process()
{
	long double angle;
	Matrix defaultT(operationMatrix.Rows(), operationMatrix.Rows(), 1), T;

	std::pair<int, int> position = getMaxAbsIndex();
	int i = position.first;
	int j = position.second;

	for (int k = 0; k < 1000 && !compareTo(operationMatrix[position], 0); k++)
	{
		T = defaultT;
		if (operationMatrix[i][i] == operationMatrix[j][j])
			angle = asin(1) / 2;
		else
			angle = 0.5 * atan2(2 * operationMatrix[position], operationMatrix[i][i] - operationMatrix[j][j]);
		T[i][i] = cos(angle);
		T[j][j] = cos(angle);
		T[i][j] = -sin(angle);
		T[j][i] = sin(angle);
		
		operationMatrix = (~T) * operationMatrix * T;
		operationMatrix.normalizeZeros();
		position = getMaxAbsIndex();
		i = position.first;
		j = position.second;
	}
}
bool JacobiEigenFinder::resultToVector(std::vector<long double>& result)
{
	if (!initialized)
		return false;
	result = std::vector<long double>(operationMatrix.Rows());
	for (int i = 0; i < operationMatrix.Rows(); i++)
	{
		result[i] = operationMatrix[i][i];
	}
	return true;
}