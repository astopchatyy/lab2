#pragma once

#include "Matrix.h"
#include "MatrixBuilder.h"
#include <cstdlib>

class SimmetrialMatrixBuilder : public MatrixBuilder
{
public:
	SimmetrialMatrixBuilder(int valueScatter = 10, int minSize = 1, int maxSize = 10);
	Matrix generateRandom();
	Matrix generateRandom(int matrixSize);
	Matrix loadFromStream(std::istream& inStream);
};

