#include "ClassifierModel.h"

ClassifierModel::ClassifierModel(RegressionModel* regression, int variablesCount) : regressionProcessor(regression), variablesCount(variablesCount) {}

void ClassifierModel::initClass(Matrix classMembers, int classIdentifier)
{
	regressionProcessor->init(classMembers);
	std::vector<long double> classVector = regressionProcessor->resultToVector();
	classVector.push_back(classIdentifier);
	classes.push_back(classVector);
}
int ClassifierModel::classify(std::vector<long double> variableValues)
{
	long double maxAnswer = -1;
	int maxAnswerIndex = -1;
	for (int i = 0; i < classes.size(); i++)
	{
		long double resultForClass = 0;
		for (int j = 0; j < variablesCount; j++)
		{
			resultForClass += variableValues[j] * classes[i][j];
		}
		if (resultForClass > maxAnswer)
		{
			maxAnswer = resultForClass;
			maxAnswerIndex = classes[i][variablesCount];
		}
	}
	return maxAnswerIndex;
}
int ClassifierModel::reviewClassifyingCorrectness(Matrix testMembers)
{
	int correctAnswersCounter = 0;
	for (int i = 0; i < testMembers.Rows(); i++)
	{
		std::vector<long double> variableValues;
		for (int j = 1; j < testMembers.Columns(); j++)
		{
			variableValues.push_back(testMembers[i][j]);
		}
		if (classify(variableValues) == testMembers[i][0])
			correctAnswersCounter++;
	}
	return correctAnswersCounter;
}