#include "DeviationGenerator.h"



DeviationGenerator::DeviationGenerator(long double deviationScatter) : deviationScatter(deviationScatter)
{
	srand((unsigned int)time(0));
}

Matrix  DeviationGenerator::deviationFromStream(std::istream& inStream)
{
	int amount, variables;
	inStream >> amount >> variables;
	Matrix result(amount, variables + 1);
	inStream >> result;
	return result;
}


Matrix DeviationGenerator::deviationFromLine(long double b, long double a, int len)
{
	Matrix result(len,2);
	long double x, y;
	for (int i = 0; i < len; i++)
	{
		x = randBetween(MINX, MAXX);
		y = a * x + b + randDoubleBetween(-deviationScatter, deviationScatter);
		result[i] = { y, x };
	}
	return result;
}
Matrix  DeviationGenerator::deviationFromMultyDem(std::vector<long double> params, int len)
{
	Matrix result(len, params.size());
	for (int i = 0; i < len; i++)
	{
		result[i][0] = params[0] + randDoubleBetween(-deviationScatter, deviationScatter);
		for (int j = 1; j < params.size(); j++)
		{
			result[i][j] = randBetween(MINX, MAXX);
			result[i][0] += result[i][j] * params[j];
		}
	}
	return result;
}