#pragma once

#include "LinearRegressionModel.h";

class ClassifierModel
{
private:
	int variablesCount;
	bool calculated;
	std::vector<std::vector<long double>> classes;
	RegressionModel* regressionProcessor;
public:
	ClassifierModel(RegressionModel* regression, int variablesCount);
	void initClass(Matrix classMembers, int classIdentifier);
	int classify(std::vector<long double> variableValues);
	int reviewClassifyingCorrectness(Matrix testMembers);

};