#include "MatrixProcessor.h"


bool MatrixProcessor::Initialized() {
	return initialized;
}
MatrixProcessor::MatrixProcessor() : operationMatrix(Matrix()), originMatrix(Matrix()), initialized(false) {}
Matrix MatrixProcessor::getResultMatrix()
{

	if (!initialized)
		throw std::logic_error("You should to init the processor");
	return operationMatrix;
}
Matrix MatrixProcessor::getOriginMatrix()
{
	if (!initialized)
		throw std::logic_error("You should to init the processor");
	return originMatrix;
}