#pragma once
#include "Matrix.h"
#include "SoLESolver.h"
#include <vector>
#include <algorithm>
#include "Functions.h"
#include <cmath>

class GaussianSoLESolverV2 : public SoLESolver
{
private:
	int curColumn, curRow;
	void process();

public:
	GaussianSoLESolverV2();
	bool init(Matrix origin);
	bool resultToVector(std::vector<long double>& result);
};

