#pragma once

#include "Matrix.h"
#include <fstream>
#include "Functions.h"

class MnistMatrixLoader
{
private:
	Matrix datasetMatrixes[10];
	bool initialized;
	int amount, rows, columns;
	const int IMAGES_CONTROL = 2051;
	const int LABELS_CONTROL = 2049;
public:
	MnistMatrixLoader();
	bool init(std::string imageDataPath, std::string imageLabelsPath);
	void initDatasetMatrixes(int matrixSizes[10]);
	Matrix operator [](int index);
};

